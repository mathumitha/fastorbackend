module.exports = {
    w_watches : {
        minPrice: 200,
        maxPrice: 50000
    },
    w_shirts : {
        minPrice: 200,
        maxPrice: 150000
    },
    w_shoes : {
        minPrice: 600,
        maxPrice: 50000
    },
    w_pants : {
        minPrice: 700,
        maxPrice: 60000
    },
    w_accessories : {
        minPrice: 100,
        maxPrice: 20000
    },
    w_tops : {
        minPrice: 400,
        maxPrice: 20000
    },
    w_jeans : {
        minPrice: 200,
        maxPrice: 2000
    },
    m_jeans : {
        minPrice: 100,
        maxPrice: 50000
    },
    m_watches : {
        minPrice: 700,
        maxPrice: 50000
    },
    m_shoes : {
        minPrice: 700,
        maxPrice: 60000
    },
    m_tshirts : {
        minPrice: 200,
        maxPrice: 40000
    },
    m_shirts : {
        minPrice: 200,
        maxPrice: 4000
    },
    m_tracks : {
        minPrice: 700,
        maxPrice: 10000
    },
    m_socks : {
        minPrice: 100,
        maxPrice: 1000
    },
    m_coat : {
        minPrice: 1500,
        maxPrice: 200000
    },
    m_pants: {
        minPrice: 1500,
        maxPrice: 20000
    },
    m_accessories: {
        minPrice: 1500,
        maxPrice: 20000
    },
    all: {
        minPrice: 100,
        maxPrice: 200000
    }
}