const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AddressSchema = mongoose.Schema({
    firstName: {type: String},
    lastName: {type: String},
    address:{
        type: String,
    },
    city:{
        type: String,
    },
    country:{
        type: String,
    },
    state:{
        type: String,
    },
    pin:{
        type: String,
    },
    cart:{type: Schema.Types.ObjectId, ref: 'Cart', required: true},
}, {
    timestamps: true
});

module.exports = mongoose.model('Address', AddressSchema);