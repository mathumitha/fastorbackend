const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const CartSchema = mongoose.Schema(
  {
    cartItem: [{ type: Schema.Types.ObjectId, ref: "CartItem" }],
    address: { type: Schema.Types.ObjectId, ref: 'Address' },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("Cart", CartSchema);
