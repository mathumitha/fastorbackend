const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CartItemSchema = mongoose.Schema({
    product: {type: Schema.Types.ObjectId, ref: 'Product'},
    quantity: {type: Number},
    cart: {type: Schema.Types.ObjectId, ref: 'Cart', required: true},
}, {
    timestamps: true
});

module.exports = mongoose.model('CartItem', CartItemSchema);