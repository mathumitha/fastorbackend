const mongoose = require('mongoose');

const CategorySchema = mongoose.Schema({
    name: {type: String, required: true},
    displayName: {type: String, required: true},
    typesInCategory: [{ type: String, ref: 'Type' }],
    productsInCategory: [{type: String, ref: 'Product'}]
}, {
    timestamps: true
});

module.exports = mongoose.model('Category', CategorySchema);