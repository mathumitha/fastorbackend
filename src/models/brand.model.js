const mongoose = require('mongoose');

const BrandSchema = mongoose.Schema({
    name: {type: String, required: true},
    displayName: {type: String, required: true},
    type: [{ type: String, ref: 'Type' }],
    productsInBrand: [{type: String, ref: 'Brand'}]
}, {
    timestamps: true
});

module.exports = mongoose.model('Brand', BrandSchema);