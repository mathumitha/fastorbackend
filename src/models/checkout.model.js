const mongoose = require("mongoose");

const CheckoutSchema = mongoose.Schema(
  {
    cart: { type: String, required: true },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("checkout", CheckoutSchema);
