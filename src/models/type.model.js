const mongoose = require('mongoose');

const TypeSchema = mongoose.Schema({
    name: {type: String, required: true},
    displayName: {type: String, required: true},
    category: [{ type: String, ref: 'Category' }],
    productsInType: [{type: String, ref: 'Product'}],
    brandsInType:  [{type: String, ref: 'Brand'}]
}, {
    timestamps: true
});

module.exports = mongoose.model('Type', TypeSchema);