const mongoose = require('mongoose');

const ProductSchema = mongoose.Schema({
    name: {type: String, required: true},
    price: {type: Number, required: true},
    originalPrice: {type: Number},
    isLatest: {type: Boolean, default: false},
    discount: {type: String},
    imageUrl: {type: String},
    category:  [{ type: String, ref: 'Category' }],
    type:  [{ type:String, ref: 'Type'}],
    brand:  [{ type:String, ref: 'Brand'}]
}, {
    timestamps: true
});

module.exports = mongoose.model('Product', ProductSchema);