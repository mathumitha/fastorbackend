const Category = require("../models/category.model.js");
const Type = require("../models/type.model.js");
// Retrieve and return all categories from the database.
exports.findAll = (req, res) => {
  const queryCat = req.query.category;
  const filterObj = queryCat ? { name: queryCat } : {};
  Category.find(filterObj)
    .then(categories => {
      const temp = categories.map(async cat => {
        cat.types = [];
        await Type.find(
          { name: { $in: cat.typesInCategory } },
          "name displayName _id"
        ).then(types => {
          cat.typesInCategory = types;
        });
        return cat;
      });
      return Promise.all(temp);
    })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message ||
          "Something went wrong while getting list of categories."
      });
    });
};
// Create and Save a new Category
exports.create = (req, res) => {
  // Validate request
  if (!req.body) {
    return res.status(400).send({
      message: "Please fill all required field"
    });
  }
  // Create a new Category
  const category = new Category({
    name: req.body.name,
    displayName: req.body.displayName
  });
  // Save category in the database
  category
    .save()
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Something went wrong while creating new category."
      });
    });
};
// Find a single Category with a id
exports.findOne = (req, res) => {
  Category.findById(req.params.id)
    .then(category => {
      if (!category) {
        return res.status(404).send({
          message: "Category not found with id " + req.params.id
        });
      }
      res.send(category);
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          message: "Category not found with id " + req.params.id
        });
      }
      return res.status(500).send({
        message: "Error getting category with id " + req.params.id
      });
    });
};
// Update a Category identified by the id in the request
exports.update = (req, res) => {
  // Validate Request
  if (!req.body) {
    return res.status(400).send({
      message: "Please fill all required field"
    });
  }
  // Find category and update it with the request body
  Category.findByIdAndUpdate(
    req.params.id,
    {
      name: req.body.name,
      displayName: req.body.displayName
    },
    { new: true }
  )
    .then(category => {
      if (!category) {
        return res.status(404).send({
          message: "category not found with id " + req.params.id
        });
      }
      res.send(category);
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          message: "category not found with id " + req.params.id
        });
      }
      return res.status(500).send({
        message: "Error updating category with id " + req.params.id
      });
    });
};
// Delete a Category with the specified id in the request
exports.delete = (req, res) => {
  Category.findByIdAndRemove(req.params.id)
    .then(category => {
      if (!category) {
        return res.status(404).send({
          message: "category not found with id " + req.params.id
        });
      }
      res.send({ message: "category deleted successfully!" });
    })
    .catch(err => {
      if (err.kind === "ObjectId" || err.name === "NotFound") {
        return res.status(404).send({
          message: "category not found with id " + req.params.id
        });
      }
      return res.status(500).send({
        message: "Could not delete category with id " + req.params.id
      });
    });
};
