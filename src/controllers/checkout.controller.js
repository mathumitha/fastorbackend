const Checkout = require("../models/checkout.model.js");
const Cart = require("../models/cart.model.js");
// Retrieve and return all checkouts from the database.
exports.findAll = (req, res) => {
  const queryType = req.query.type;
  const filterObj = queryType ? { type: queryType } : {};
  Checkout.find(filterObj)
    .then(checkouts => {
      res.send(checkouts);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Something went wrong while getting list of checkouts."
      });
    });
};
// Create and Save a new Checkout
exports.create = (req, res) => {
    // Validate request
    if (!req.body) {
      return res.status(400).send({
        message: "Please fill all required field"
      });
    }
    // Create a new Type
    const checkout = new Checkout({
        cart: req.body.cartId
    });
    // Save type in the database
    checkout
      .save()
      .then(function(checkout) {
        Cart.findByIdAndRemove(req.body.cartId)
        .then((cart) => {
          if (!cart) {
            return res.status(404).send({
              message: "cart not found with id " + req.params.id,
            });
          }
          res.send(checkout);
        })
      })
      .catch(err => {
        res.status(500).send({
          message: err.message || "Something went wrong while creating new checkout."
        });
      });
  };
// Find a single Checkout with a id
exports.findOne = (req, res) => {
  Checkout.findById(req.params.id)
    .then(checkout => {
      if (!checkout) {
        return res.status(404).send({
          message: "Checkout not found with id " + req.params.id
        });
      }
      res.send(checkout);
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          message: "Checkout not found with id " + req.params.id
        });
      }
      return res.status(500).send({
        message: "Error getting checkout with id " + req.params.id
      });
    });
};
// Update a Checkout identified by the id in the request
exports.update = (req, res) => {
  // Validate Request
  if (!req.body) {
    return res.status(400).send({
      message: "Please fill all required field"
    });
  }
  // Find checkout and update it with the request body
  Checkout.findByIdAndUpdate(
    req.params.id,
    {
        name: req.body.name,
        displayName: req.body.displayName
    },
    { new: true }
  )
    .then(checkout => {
      if (!checkout) {
        return res.status(404).send({
          message: "checkout not found with id " + req.params.id
        });
      }
      res.send(checkout);
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          message: "checkout not found with id " + req.params.id
        });
      }
      return res.status(500).send({
        message: "Error updating checkout with id " + req.params.id
      });
    });
};
// Delete a Checkout with the specified id in the request
exports.delete = (req, res) => {
  Checkout.findByIdAndRemove(req.params.id)
    .then(checkout => {
      if (!checkout) {
        return res.status(404).send({
          message: "checkout not found with id " + req.params.id
        });
      }
      res.send({ message: "checkout deleted successfully!" });
    })
    .catch(err => {
      if (err.kind === "ObjectId" || err.name === "NotFound") {
        return res.status(404).send({
          message: "checkout not found with id " + req.params.id
        });
      }
      return res.status(500).send({
        message: "Could not delete checkout with id " + req.params.id
      });
    });
};
