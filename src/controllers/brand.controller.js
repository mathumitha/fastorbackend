const Brand = require("../models/brand.model.js");
const Type = require("../models/type.model.js");
// Retrieve and return all brands from the database.
exports.findAll = (req, res) => {
  const queryType = req.query.type;
  const filterObj = queryType ? { type: queryType } : {};
  Brand.find(filterObj)
    .then(brands => {
      res.send(brands);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Something went wrong while getting list of brands."
      });
    });
};
// Create and Save a new Brand
exports.create = (req, res) => {
    // Validate request
    if (!req.body) {
      return res.status(400).send({
        message: "Please fill all required field"
      });
    }
    // Create a new Type
    const brand = new Brand({
      name: req.body.name,
      displayName: req.body.displayName,
      type: req.body.type
    });
    // Save type in the database
    brand
      .save()
      .then(data => {
          return Type.findOneAndUpdate({ name: req.body.type}, {$push: {brandsInType: brand.name}}, { new: true } );
      })
      .then(function(type) {
          res.send(brand);
      })
      .catch(err => {
        res.status(500).send({
          message: err.message || "Something went wrong while creating new brand."
        });
      });
  };
// Find a single Brand with a id
exports.findOne = (req, res) => {
  Brand.findById(req.params.id)
    .then(brand => {
      if (!brand) {
        return res.status(404).send({
          message: "Brand not found with id " + req.params.id
        });
      }
      res.send(brand);
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          message: "Brand not found with id " + req.params.id
        });
      }
      return res.status(500).send({
        message: "Error getting brand with id " + req.params.id
      });
    });
};
// Update a Brand identified by the id in the request
exports.update = (req, res) => {
  // Validate Request
  if (!req.body) {
    return res.status(400).send({
      message: "Please fill all required field"
    });
  }
  // Find brand and update it with the request body
  Brand.findByIdAndUpdate(
    req.params.id,
    {
        name: req.body.name,
        displayName: req.body.displayName
    },
    { new: true }
  )
    .then(brand => {
      if (!brand) {
        return res.status(404).send({
          message: "brand not found with id " + req.params.id
        });
      }
      res.send(brand);
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          message: "brand not found with id " + req.params.id
        });
      }
      return res.status(500).send({
        message: "Error updating brand with id " + req.params.id
      });
    });
};
// Delete a Brand with the specified id in the request
exports.delete = (req, res) => {
  Brand.findByIdAndRemove(req.params.id)
    .then(brand => {
      if (!brand) {
        return res.status(404).send({
          message: "brand not found with id " + req.params.id
        });
      }
      res.send({ message: "brand deleted successfully!" });
    })
    .catch(err => {
      if (err.kind === "ObjectId" || err.name === "NotFound") {
        return res.status(404).send({
          message: "brand not found with id " + req.params.id
        });
      }
      return res.status(500).send({
        message: "Could not delete brand with id " + req.params.id
      });
    });
};
