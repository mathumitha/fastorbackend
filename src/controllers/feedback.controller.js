const Feedback = require("../models/feedback.model.js");
const Cart = require("../models/cart.model.js");
// Retrieve and return all feedback from the database.
exports.findAll = (req, res) => {
  Feedback.find()
    .then(feedback => {
      res.send(feedback);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Something went wrong while getting list of feedback."
      });
    });
};
// Create and Save a new feedback
exports.create = (req, res) => {
    // Validate request
    if (!req.body) {
      return res.status(400).send({
        message: "Please fill all required field"
      });
    }
    // Create a new Feedback
    const feedback = new Feedback({
        name: req.body.name,
      email: req.body.email,
      subject: req.body.subject,
      message: req.body.message,
    });
    feedback
      .save()
      .then(function(feedback) {
        res.send(feedback);
      })
      .catch(err => {
        res.status(500).send({
          message: err.message || "Something went wrong while creating new feedback."
        });
      });
  };
// Find a single feedback with a id
exports.findOne = (req, res) => {
    Feedback.findById(req.params.id)
    .then(feedback => {
      if (!feedback) {
        return res.status(404).send({
          message: "feedback not found with id " + req.params.id
        });
      }
      res.send(feedback);
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          message: "feedback not found with id " + req.params.id
        });
      }
      return res.status(500).send({
        message: "Error getting feedback with id " + req.params.id
      });
    });
};
// Update a feedback identified by the id in the request
exports.update = (req, res) => {
  // Validate Request
  if (!req.body) {
    return res.status(400).send({
      message: "Please fill all required field"
    });
  }
  // Find feedback and update it with the request body
  Feedback.findByIdAndUpdate(
    req.params.id,
    {
        name: req.body.name,
        displayName: req.body.displayName
    },
    { new: true }
  )
    .then(feedback => {
      if (!feedback) {
        return res.status(404).send({
          message: "feedback not found with id " + req.params.id
        });
      }
      res.send(feedback);
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          message: "feedback not found with id " + req.params.id
        });
      }
      return res.status(500).send({
        message: "Error updating feedback with id " + req.params.id
      });
    });
};
// Delete a feedback with the specified id in the request
exports.delete = (req, res) => {
    Feedback.findByIdAndRemove(req.params.id)
    .then(feedback => {
      if (!feedback) {
        return res.status(404).send({
          message: "feedback not found with id " + req.params.id
        });
      }
      res.send({ message: "feedback deleted successfully!" });
    })
    .catch(err => {
      if (err.kind === "ObjectId" || err.name === "NotFound") {
        return res.status(404).send({
          message: "feedback not found with id " + req.params.id
        });
      }
      return res.status(500).send({
        message: "Could not delete feedback with id " + req.params.id
      });
    });
};
