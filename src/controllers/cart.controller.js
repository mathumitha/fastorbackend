const Cart = require("../models/cart.model.js");
const CartItem = require("../models/cart-item.model.js");
const Address = require("../models/address.model.js");
const Product = require("../models/product.model.js");
// Retrieve and return all cart from the database.
exports.findAll = (req, res) => {
  Cart.find()
    .then((cart) => {
      const temp = cart.map(async (item) => {
        await CartItem.find({ _id: { $in: item.cartItem } }).then(
          (cartItems) => {
            item.cartItem = cartItems;
          }
        );
        return item;
      });
      return Promise.all(temp);
    })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Something went wrong while getting list of cart.",
      });
    });
};
// Create and Save a new cart
exports.create = (req, res) => {
  // Validate request
  if (!req.body) {
    return res.status(400).send({
      message: "Please fill all required field",
    });
  }

  const cart = new Cart();
  // Save Cart in the database
  cart
    .save()
    .then(function (cartRes) {
      res.send(cartRes);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Something went wrong while creating new cart.",
      });
    });
};

exports.addProductToCart = (req, res) => {
  Cart.findById(req.params.id)
    .then((cart) => {
      if (!cart) {
        return res.status(404).send({
          message: "cart not found with id " + req.params.id,
        });
      }
      // Create a new cartItem
      const cartItem = new CartItem({
        product: req.body.productId,
        quantity: req.body.quantity,
        cart: cart._id,
      });
      cartItem
        .save()
        .then(function (cartItemRes) {
          return Cart.findOneAndUpdate(
            { _id: req.params.id },
            { $push: { cartItem: cartItemRes._id } },
            { new: true }
          );
        })
        .then((cartRes) => {
          res.json(cartRes);
        });
    })
    .catch((err) => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          message: "cart not found with id " + req.params.id,
        });
      }
      return res.status(500).send({
        message: "Error getting cart with id " + req.params.id,
      });
    });
};

exports.addAddressToCart = (req, res) => {
  Cart.findById(req.params.id)
    .then((cart) => {
      if (!cart) {
        return res.status(404).send({
          message: "cart not found with id " + req.params.id,
        });
      }
      // Create a new cartItem
      const address = new Address({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        address: req.body.address,
        city: req.body.city,
        country: req.body.country,
        state: req.body.state,
        pin: req.body.pin,
        cart: cart._id,
      });
      address
        .save()
        .then(function (addRes) {
          return Cart.findOneAndUpdate(
            { _id: cart._id },
            { $set: { address: addRes._id } },
            { new: true }
          );
        })
        .then((addRes) => {
          res.json({ message: "Address Added" });
        });
    })
    .catch((err) => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          message: "cart not found with id " + req.params.id,
        });
      }
      return res.status(500).send({
        message: "Error getting cart with id " + req.params.id,
      });
    });
};

// Find a single cart with a id
exports.findOne = (req, res) => {
  Cart.findById(req.params.id)
    .then((cart) => {
        return CartItem.find({ _id: { $in: cart.cartItem } });
    })
    .then((cartItems) => {
        const temp = cartItems.map(async (item) => {
          await Product.findOne({ _id: item.product }).then(
            (product) => {
              item.product = product;
            }
          );
          return item;
        });
        return Promise.all(temp);
      })
    .then((data) => {
        Address.findOne({
            cart: req.params.id
        }).then(function(address){
            res.send({address, items: data});
        })
    })
    .catch((err) => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          message: "cart not found with id " + req.params.id,
        });
      }
      return res.status(500).send({
        message: "Error getting cart with id " + req.params.id,
      });
    });
};
// Update a cart identified by the id in the request
exports.updateQuantity = (req, res) => {
  // Validate Request
  if (!req.body) {
    return res.status(400).send({
      message: "Please fill all required field",
    });
  }
  // Find cart and update it with the request body
  Cart.findById(req.params.id)
    .then((cart) => {
      if (!cart) {
        return res.status(404).send({
          message: "cart not found with id " + req.params.id,
        });
      }
      return CartItem.findOneAndUpdate(
        { product: req.body.productId },
        { $set: { quantity:  req.body.quantity } }
      )
    })
    .then(function(data) {
        res.send(data);
    })
    .catch((err) => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          message: "cart not found with id " + req.params.id,
        });
      }
      return res.status(500).send({
        message: "Error updating cart with id " + req.params.id,
      });
    });
};
// Delete a cart with the specified id in the request
exports.delete = (req, res) => {
  Cart.findByIdAndRemove(req.params.id)
    .then((cart) => {
      if (!cart) {
        return res.status(404).send({
          message: "cart not found with id " + req.params.id,
        });
      }
      res.send({ message: "cart deleted successfully!" });
    })
    .catch((err) => {
      if (err.kind === "ObjectId" || err.name === "NotFound") {
        return res.status(404).send({
          message: "cart not found with id " + req.params.id,
        });
      }
      return res.status(500).send({
        message: "Could not delete cart with id " + req.params.id,
      });
    });
};
