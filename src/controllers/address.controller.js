const Address = require("../models/address.model.js");
const Cart = require("../models/cart.model.js");
// Retrieve and return all address from the database.
exports.findAll = (req, res) => {
  Address.find()
    .then(address => {
      res.send(address);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Something went wrong while getting list of address."
      });
    });
};
// Create and Save a new address
exports.create = (req, res) => {
    // Validate request
    if (!req.body) {
      return res.status(400).send({
        message: "Please fill all required field"
      });
    }
    // Create a new Address
    const address = new Address({
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      address: req.body.address,
      city: req.body.city,
      country: req.body.country,
      state: req.body.state,
      pin: req.body.pin,
      cart: cart._id,
    });
    address
      .save()
      .then(function(addRes) {
        return Cart.findOneAndUpdate({ _id: cart._id}, {$push: {address:addRes._id}}, { new: true } );
      })
      .then(function(address) {
        res.send(cartItem);
      })
      .catch(err => {
        res.status(500).send({
          message: err.message || "Something went wrong while creating new address."
        });
      });
  };
// Find a single address with a id
exports.findOne = (req, res) => {
    Address.findById(req.params.id)
    .then(address => {
      if (!address) {
        return res.status(404).send({
          message: "address not found with id " + req.params.id
        });
      }
      res.send(address);
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          message: "address not found with id " + req.params.id
        });
      }
      return res.status(500).send({
        message: "Error getting address with id " + req.params.id
      });
    });
};
// Update a address identified by the id in the request
exports.update = (req, res) => {
  // Validate Request
  if (!req.body) {
    return res.status(400).send({
      message: "Please fill all required field"
    });
  }
  // Find address and update it with the request body
  Address.findByIdAndUpdate(
    req.params.id,
    {
        name: req.body.name,
        displayName: req.body.displayName
    },
    { new: true }
  )
    .then(address => {
      if (!address) {
        return res.status(404).send({
          message: "address not found with id " + req.params.id
        });
      }
      res.send(address);
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          message: "address not found with id " + req.params.id
        });
      }
      return res.status(500).send({
        message: "Error updating address with id " + req.params.id
      });
    });
};
// Delete a address with the specified id in the request
exports.delete = (req, res) => {
    Address.findByIdAndRemove(req.params.id)
    .then(address => {
      if (!address) {
        return res.status(404).send({
          message: "address not found with id " + req.params.id
        });
      }
      res.send({ message: "address deleted successfully!" });
    })
    .catch(err => {
      if (err.kind === "ObjectId" || err.name === "NotFound") {
        return res.status(404).send({
          message: "address not found with id " + req.params.id
        });
      }
      return res.status(500).send({
        message: "Could not delete address with id " + req.params.id
      });
    });
};
