const Type = require("../models/type.model.js");
const Category = require("../models/category.model.js");
const Brand = require("../models/brand.model.js");
// Retrieve and return all types from the database.
exports.findAll = (req, res) => {
  Type.find()
    .then(types => {
      const temp = types.map(async type => {
        type.brands = [];
        await Brand.find(
          { name: { $in: type.brandsInType } },
          "name displayName _id"
        ).then(brands => {
          type.brandsInType = brands;
        });
        return type;
      });
      return Promise.all(temp);
    })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Something went wrong while getting list of types."
      });
    });
};
// Create and Save a new Type
exports.create = (req, res) => {
  // Validate request
  if (!req.body) {
    return res.status(400).send({
      message: "Please fill all required field"
    });
  }
  // Create a new Type
  const type = new Type({
    name: req.body.name,
    displayName: req.body.displayName,
    category: req.body.category
  });
  // Save type in the database
  type
    .save()
    .then(data => {
        return Category.findOneAndUpdate({ name: req.body.category}, {$push: {typesInCategory: type.name}}, { new: true } );
    })
    .then(function() {
        res.send(type);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || "Something went wrong while creating new type."
      });
    });
};
// Find a single Type with a id
exports.findOne = (req, res) => {
  Type.findById(req.params.id)
    .then(type => {
      if (!type) {
        return res.status(404).send({
          message: "Type not found with id " + req.params.id
        });
      }
      res.send(type);
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          message: "Type not found with id " + req.params.id
        });
      }
      return res.status(500).send({
        message: "Error getting type with id " + req.params.id
      });
    });
};
// Update a Type identified by the id in the request
exports.update = (req, res) => {
  // Validate Request
  if (!req.body) {
    return res.status(400).send({
      message: "Please fill all required field"
    });
  }
  // Find type and update it with the request body
  Type.findByIdAndUpdate(
    req.params.id,
    {
        name: req.body.name,
        displayName: req.body.displayName
    },
    { new: true }
  )
    .then(type => {
      if (!type) {
        return res.status(404).send({
          message: "type not found with id " + req.params.id
        });
      }
      res.send(type);
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          message: "type not found with id " + req.params.id
        });
      }
      return res.status(500).send({
        message: "Error updating type with id " + req.params.id
      });
    });
};
// Delete a Type with the specified id in the request
exports.delete = (req, res) => {
  Type.findByIdAndRemove(req.params.id)
    .then(type => {
      if (!type) {
        return res.status(404).send({
          message: "type not found with id " + req.params.id
        });
      }
      res.send({ message: "type deleted successfully!" });
    })
    .catch(err => {
      if (err.kind === "ObjectId" || err.name === "NotFound") {
        return res.status(404).send({
          message: "type not found with id " + req.params.id
        });
      }
      return res.status(500).send({
        message: "Could not delete type with id " + req.params.id
      });
    });
};

// Delete all
exports.deleteAll = (req, res) => {
    Type.remove({})
      .then(type => {
        if (!type) {
          return res.status(404).send({
            message: "Deleted"
          });
        }
        res.send({ message: "type deleted successfully!" });
      })
      .catch(err => {
        return res.status(500).send({
          message: "Could not delete type"
        });
      });
  };
