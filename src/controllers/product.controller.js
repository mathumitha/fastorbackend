const Product = require("../models/product.model.js");
const Type = require("../models/type.model.js");
const Category = require("../models/category.model.js");
const Brand = require("../models/brand.model.js");
const priceRange = require('../../constants/price-range');

// Retrieve and return all products from the database.
exports.findAll = (req, res) => {
  const queryType = req.query.type || '';
  const queryCat = req.query.category || '';
  const queryBrand = req.query.brand || '';
  const typeObj = queryType ? { type: queryType } : {};
  const catObj = queryCat ? { category: queryCat } : {};
  const brandObj = queryBrand ? { brand: {$in: queryBrand.split(',')} } : [];
  let priceRangeOfQuery = queryType ? priceRange[queryType] : priceRange['all'];
  const queryMinPrice = req.query.minPrice === undefined ? 0 : req.query.minPrice ;
  const queryMaxPrice = req.query.maxPrice === undefined ? priceRangeOfQuery.maxPrice : req.query.maxPrice;
  const priceLimitObj = queryMinPrice && queryMaxPrice ?  { price: { $gte:queryMinPrice, $lte: queryMaxPrice }} :{}

  Product.find({...typeObj, ...catObj, ...brandObj, ...priceLimitObj })
    .then(products => {
      productList = {
        products,
        priceRange: priceRangeOfQuery
      }
      res.send(productList);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Something went wrong while getting list of products."
      });
    });
};
// Get Latest Products
exports.latest = (req, res) => {
  limit = Number(req.query.limit) || 8;

  Product.find({'isLatest': true}).sort({_id:-1}).limit(limit)
    .exec((err, products) => {
      res.send(products);
    })
    // .catch(err => {
    //   res.status(500).send({
    //     message:
    //       err.message || "Something went wrong while getting list of products."
    //   });
    // });
};

// Create and Save a new Product
exports.create = (req, res) => {
  // Validate request
  if (!req.body) {
    return res.status(400).send({
      message: "Please fill all required field"
    });
  }
  // Create a new Product
  const product = new Product({
    name: req.body.name,
    price: req.body.price,
    originalPrice: req.body.originalPrice,
    isLatest: req.body.isLatest,
    discount: req.body.discount,
    imageUrl: req.body.imageUrl,
    category: req.body.category,
    type: req.body.type,
    brand: req.body.brand,
  });
  // Save product in the database
  product
    .save()
    .then(data => {
        return Category.findOneAndUpdate({ name: req.body.category}, {$push: {productsInCategory: product.name}}, { new: true } );
    })
    .then(data => {
        return Type.findOneAndUpdate({ name: req.body.type}, {$push: {productsInType: product.name}}, { new: true } );
    })
    .then(data => {
        return Brand.findOneAndUpdate({ name: req.body.brand}, {$push: {productsInBrand: product.name}}, { new: true } );
    })
    .then(function(dsta) {
        res.send(product);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || "Something went wrong while creating new product."
      });
    });
};
// Find a single Product with a id
exports.findOne = (req, res) => {
  Product.findById(req.params.id)
    .then(product => {
      if (!product) {
        return res.status(404).send({
          message: "Product not found with id " + req.params.id
        });
      }
      res.send(product);
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          message: "Product not found with id " + req.params.id
        });
      }
      return res.status(500).send({
        message: "Error getting product with id " + req.params.id
      });
    });
};
// Update a Product identified by the id in the request
exports.update = (req, res) => {
  // Validate Request
  if (!req.body) {
    return res.status(400).send({
      message: "Please fill all required field"
    });
  }
  // Find product and update it with the request body
  Product.findByIdAndUpdate(
    req.params.id,
    {
        name: req.body.name,
        displayName: req.body.displayName
    },
    { new: true }
  )
    .then(product => {
      if (!product) {
        return res.status(404).send({
          message: "product not found with id " + req.params.id
        });
      }
      res.send(product);
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          message: "product not found with id " + req.params.id
        });
      }
      return res.status(500).send({
        message: "Error updating product with id " + req.params.id
      });
    });
};
// Delete a Product with the specified id in the request
exports.delete = (req, res) => {
  Product.findByIdAndRemove(req.params.id)
    .then(product => {
      if (!product) {
        return res.status(404).send({
          message: "product not found with id " + req.params.id
        });
      }
      res.send({ message: "product deleted successfully!" });
    })
    .catch(err => {
      if (err.kind === "ObjectId" || err.name === "NotFound") {
        return res.status(404).send({
          message: "product not found with id " + req.params.id
        });
      }
      return res.status(500).send({
        message: "Could not delete product with id " + req.params.id
      });
    });
};

// Delete all
exports.deleteAll = (req, res) => {
    Product.remove({})
      .then(product => {
        if (!product) {
          return res.status(404).send({
            message: "Deleted"
          });
        }
        res.send({ message: "product deleted successfully!" });
      })
      .catch(err => {
        return res.status(500).send({
          message: "Could not delete product"
        });
      });
  };
