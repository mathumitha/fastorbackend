const CartItem = require("../models/cart-item.model.js");
const Cart = require("../models/cart.model.js");
// Retrieve and return all cartItem from the database.
exports.findAll = (req, res) => {
  CartItem.find()
    .then(cartItem => {
      res.send(cartItem);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Something went wrong while getting list of cartItem."
      });
    });
};
// Create and Save a new cartItem
exports.create = (req, res) => {
    // Validate request
    if (!req.body) {
      return res.status(400).send({
        message: "Please fill all required field"
      });
    }
    // Create a new Type
    const cartItem = new CartItem({
      cartItem: req.body.productId,
      quantity: req.body.quantity,
      cart: cart._id,
    });
    // Save type in the database
    cartItem
      .save()
      .then(function(cartItemRes) {
        return Cart.findOneAndUpdate({ _id: cart._id}, {$push: {cartItem:cartItemRes._id}}, { new: true } );
      })
      .then(function(cartItem) {
        res.send(cartItem);
      })
      .catch(err => {
        res.status(500).send({
          message: err.message || "Something went wrong while creating new cartItem."
        });
      });
  };
// Find a single cartItem with a id
exports.findOne = (req, res) => {
    CartItem.findById(req.params.id)
    .then(cartItem => {
      if (!cartItem) {
        return res.status(404).send({
          message: "cartItem not found with id " + req.params.id
        });
      }
      res.send(cartItem);
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          message: "cartItem not found with id " + req.params.id
        });
      }
      return res.status(500).send({
        message: "Error getting cartItem with id " + req.params.id
      });
    });
};
// Update a cartItem identified by the id in the request
exports.update = (req, res) => {
  // Validate Request
  if (!req.body) {
    return res.status(400).send({
      message: "Please fill all required field"
    });
  }
  // Find cartItem and update it with the request body
  CartItem.findByIdAndUpdate(
    req.params.id,
    {
        name: req.body.name,
        displayName: req.body.displayName
    },
    { new: true }
  )
    .then(cartItem => {
      if (!cartItem) {
        return res.status(404).send({
          message: "cartItem not found with id " + req.params.id
        });
      }
      res.send(cartItem);
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          message: "cartItem not found with id " + req.params.id
        });
      }
      return res.status(500).send({
        message: "Error updating cartItem with id " + req.params.id
      });
    });
};
// Delete a cartItem with the specified id in the request
exports.delete = (req, res) => {
    CartItem.findByIdAndRemove(req.params.id)
    .then(cartItem => {
      if (!cartItem) {
        return res.status(404).send({
          message: "cartItem not found with id " + req.params.id
        });
      }
      res.send({ message: "cartItem deleted successfully!" });
    })
    .catch(err => {
      if (err.kind === "ObjectId" || err.name === "NotFound") {
        return res.status(404).send({
          message: "cartItem not found with id " + req.params.id
        });
      }
      return res.status(500).send({
        message: "Could not delete cartItem with id " + req.params.id
      });
    });
};
