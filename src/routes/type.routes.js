const express = require('express')
const router = express.Router();
const typeController = require('../controllers/type.controller');
// Retrieve all types
router.get('/', typeController.findAll);
// Create a new type
router.post('/', typeController.create);
// Retrieve a single type with id
router.get('/:id', typeController.findOne);
// Update a type with id
router.put('/:id', typeController.update);
// Delete a type with id
router.delete('/:id', typeController.delete);
// Delete all types
router.delete('/', typeController.deleteAll);
module.exports = router