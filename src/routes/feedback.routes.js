const express = require('express')
const router = express.Router();
const feedbackController = require('../controllers/feedback.controller');
// Retrieve all feedbacks
router.get('/', feedbackController.findAll);
// Create a new feedback
router.post('/', feedbackController.create);
// Retrieve a single feedback with id
router.get('/:id', feedbackController.findOne);
// Update a feedback with id
router.put('/:id', feedbackController.update);
// Delete a feedback with id
router.delete('/:id', feedbackController.delete);
module.exports = router