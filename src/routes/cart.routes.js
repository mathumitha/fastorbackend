const express = require('express')
const router = express.Router();
const cartController = require('../controllers/cart.controller');
// Retrieve all categories
router.get('/', cartController.findAll);
// Create a new cart
router.post('/', cartController.create);
// Retrieve a single cart with id
router.get('/:id', cartController.findOne);
// Add cart item to a single cart with id
router.post('/:id/addToCart', cartController.addProductToCart);
// Add address
router.post('/:id/addAddressToCart', cartController.addAddressToCart);
// Update quantity
router.post('/:id/updateQuantity', cartController.updateQuantity);
// Delete a cart with id
router.delete('/:id', cartController.delete);
module.exports = router