const express = require('express')
const router = express.Router();
const productController = require('../controllers/product.controller');
// Retrieve all products
router.get('/', productController.findAll);
// Create a new product
router.post('/', productController.create);
// Get Latest products
router.get('/latest', productController.latest);
// Retrieve a single product with id
router.get('/:id', productController.findOne);
// Update a product with id
router.put('/:id', productController.update);
// Delete a product with id
router.delete('/:id', productController.delete);
// Delete all products
router.delete('/', productController.deleteAll);
module.exports = router