const express = require('express')
const router = express.Router();
const checkoutController = require('../controllers/checkout.controller');
// Retrieve all checkouts
router.get('/', checkoutController.findAll);
// Create a new checkout
router.post('/', checkoutController.create);
// Retrieve a single checkout with id
router.get('/:id', checkoutController.findOne);
// Update a checkout with id
router.put('/:id', checkoutController.update);
// Delete a checkout with id
router.delete('/:id', checkoutController.delete);
module.exports = router